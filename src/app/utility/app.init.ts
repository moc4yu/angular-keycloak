
import { KeycloakService } from 'keycloak-angular';
import { environment } from 'src/environments/environment';

export function initializeKeycloak(keycloak: KeycloakService): () => Promise<boolean> {
    return () =>
        keycloak.init({
            config: {
                //url: 'http://localhost:8080/auth', //'https://sso.lpdb.id',
                url: environment.ssoEndpoint,
                realm: environment.realm,
                clientId: environment.clientId,
                // url: 'https://dev-account.kasn.go.id/auth',
                // realm: 'kasn',
                // clientId: 'instansi',
            },
            initOptions: {
                checkLoginIframe: true,
                checkLoginIframeInterval: 25,
                //onLoad: 'check-sso',
                //silentCheckSsoRedirectUri: window.location.origin
            },
            loadUserProfileAtStartUp: true
        });
}
