import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  user = '';

  constructor(private keycloakService: KeycloakService, private router: Router) { }

  ngOnInit(): void {
    this.initializeUserOptions();
  }

  private initializeUserOptions(): void {
    this.user = this.keycloakService.getUsername();
  }

  logout() {
    const idToken = this.keycloakService.getKeycloakInstance().idToken;
    const redirectUri = environment.url;
    const openIdLogoutUrl = `${environment.ssoEndpoint}/realms/${environment.realm}/protocol/openid-connect/logout`;
    const logoutUrl = `${openIdLogoutUrl}?id_token_hint=${idToken}&post_logout_redirect_uri=${redirectUri}`;
    window.location.href = logoutUrl;
  }

}

